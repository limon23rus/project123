class Clock {
    constructor() {
        this.start = document.querySelector('.start-btn');
        this.stop = document.querySelector('.stop-btn')
        this.clock = document.querySelector('.clock');

        this.timeOn();
        this.startListener();
        this.stopListener();
    }

    timeOn() {
        console.log(this.clock)
        this.setTime()
        this.timeGo = setInterval(() => {
            this.setTime();
        }, 1000);
    }

    setTime() {
        this.clock.innerHTML = new Date().toLocaleTimeString();
    }

    timeOff() {
        clearInterval(this.timeGo);
    }

    startListener() {
        this.start.addEventListener('click', () => {
            this.timeOn();
            new MessageController('Часы', 'Время вновь пошло');

        })
    }

    stopListener() {
        this.stop.addEventListener('click', () => {
            this.timeOff();
            new MessageController('Часы', 'Время остановилось');
        })
    }
}

class Users {
    constructor() {
        this.userlist = [];
    }


}

class UserController {
    constructor(login, password) {
        this.addUser(login, password);
    }

    addUser(name, pass) {
        users.userlist.push({Login: name, Password: pass});
        console.log(users.userlist);
    }
}

class Registration {
    constructor(form) {
        this.regForm = form;
        this.regName = form.querySelector('.input.name-reg');
        this.regPass = form.querySelector('.input.pass-reg');
        this.addListener();
    }

    addListener() {
        this.regForm.addEventListener('submit', (event) => {
            event.preventDefault();
            this.formValidator();
        })
    }

    formValidator() {
        if (this.regName.value && this.regPass.value && this.regPass.value.length > 2) {
            this.success();
        } else {
            this.wrongData();
        }
    }

    success() {
        new UserController(this.regName.value, this.regPass.value)
        new MessageController('Регистрация',
            'Вы успешно зарегестрированны как: ' + this.regName.value);
        this.clearForm();
    }

    wrongData() {
        new MessageController('Неудачная регистрация',
            'Введите имя и пароль(не менее 3 символов)');
        this.clearForm();
    }

    clearForm() {
        this.regName.value = '';
        this.regPass.value = '';
    }
}

class Login {
    constructor(form) {
        this.logForm = form;
        this.exitBtn = form.querySelector('.exit-btn');
        this.logBtn = form.querySelector('.login-btn');
        this.logName = form.querySelector('.name-login');
        this.logPass = form.querySelector('.pass-login');
        this.startBtn = document.querySelector('.start-btn');
        this.stopBtn = document.querySelector('.stop-btn');
        this.time = document.querySelector('.clock');
        this.addListener();
    }

    addListener() {
        this.logForm.addEventListener('submit', (event) => {
            event.preventDefault();
            this.verification();
        })
    }

    verification() {
        if (this.findUser()) {
            this.success()
        } else this.wrongLog()
    }

    success() {
        this.logIn();
        new MessageController('Успешный вход',
            'Вы вошли как:' + this.logName.value);
        this.clearForm();
    }

    wrongLog() {
        new MessageController('Неудачная попытка входа',
            'Неверное имя пользователя или пароль');
        this.clearForm();
    }

    clearForm() {
        this.logName.value = '';
        this.logPass.value = '';
    }

    logIn() {
        this.startBtn.classList.add('enable');
        this.stopBtn.classList.add('enable');
        this.time.classList.add('enable');
    }

    findUser() {
        return users.userlist.some((user) => {
            return user.Login === this.logName.value && user.Password === this.logPass.value;
        })
    }
}

class MessageController {
    constructor(msgName, msgText) {
        this.messageName = document.querySelector('.message-name');
        this.messageText = document.querySelector('.message-text');
        this.msgModule = document.querySelector('.notification-module');
        this.showMsgModule();
        this.render(msgName, msgText);
    }

    displayMsg() {

    }

    showMsgModule() {
        this.msgModule.classList.remove('unable')
        this.hideMsgModule();
    }

    hideMsgModule() {
        setTimeout(() => {
            this.msgModule.classList.add('unable');
        }, 3000)
    }

    render(msgName, msgText) {
        this.messageName.innerHTML = msgName;
        this.messageText.innerHTML = msgText;
    }
}

let users = new Users();
new Registration(document.querySelector('.registration-module'));
new Login(document.querySelector('.login-module'));
new Clock;
